// Function
/*
 	Syntax:
 	 	function functionName(){
		code block (statement)
 	 	};
*/

function printName(){
	console.log("My name is Kenneth");
}

printName();

//  Function declaration vs expressions

// Function declaration
function declaredFunction(){
	console.log("Hello, world from declaredfunction()");
};

declaredFunction();

// Function expression
let variableFunction = function(){
	console.log("Hello again!");
}

variableFunction();

declaredFunction = function(){
	console.log("Updated declaredFunction")
}

declaredFunction();

const constantFunc = function(){
	console.log("Initialized with const!")
}

constantFunc();

/*constantFunc = function(){
	console.log("Cannot be re-assigned")
}

constantFunc();*/

// Function scoping 
/*
   Scope is the accesibility (visibility) of variables

	JS Variables has 3 types of scope:
	 	1. local/block scope
	 	2. global scope
	 	3. function scope
*/
// local block
{
	let localVar = "Alonzo Mattheo";
	console.log(localVar)
}
// global
let globalVar = "Aizaac Ellis"

console.log(globalVar);

// function scope
function showNames() {
	// Function scoped variables
	var functionVar = "Joe"
	const functionConst ="John"
	let functionLet = "Jane"

	console.log(functionVar)
	console.log(functionConst)
	console.log(functionLet)
}	

showNames();

// Nested Functions

function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John"
		console.log(name)
	}
	nestedFunction();
}
myNewFunction();
// Function and Global scoped Variables
// Global scoped variable
let globalName = "Joy"

function myNewFunction2() {
	let nameInside = "Kenzo"

	console.log(globalName)
	console.log(nameInside)
}

myNewFunction2()

//  Using alert

alert("Hello, World!");

function showSampleAlert(){
	alert("Hello, User!");
}

showSampleAlert();

// using prompt()
let samplePrompt = prompt("Enter your name: ")

console.log("Hello, " + samplePrompt);


function printWelcomeMessage(){
	let firstName = prompt("Enter your first name: ")
	let lastName = prompt("Enter your last name: ")

	console.log("Hello, " +firstName + " " + lastName + "!")
	console.log("welcome to my page!")
}

printWelcomeMessage();

// Function naming conventions
/*
	- Functions names should be definitive of the task it will perform
	- Avoid generic names to avoid confusion within the code
	- Avoid pointless and inappropriate function names
	- Name your functions follow camel casing.
	- Do not use JS reserved keywords
*/