console.log("Hello World!")
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function printInfo(){
		let fullName = prompt("Enter your Full Name")
		let age = prompt("Enter your age")
		let location = prompt("Enter your location")
		alert("Thankyou for Entering your info");
		console.log("Hello "+ fullName)
		console.log("You are "+ age +" years old")
		console.log("You live in "+ location)
	}
	printInfo()
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function showMyFavoriteBand(){
		let band1 = "Eraserheads"
		let band2 = "Parokya Ni Edgar"
		let band3 = "Boyce Avenue"
		let band4 = "December Avenue"
		let band5 = "Silent Sanctuary"
		console.log("1. " + band1)
		console.log("2. " + band2)
		console.log("3. " + band3)
		console.log("4. " + band4)
		console.log("5. " + band5)
	}
	showMyFavoriteBand()
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function showMyFavoriteMovies(){
		let movie1 = "The Godfather"
		let movie2 = "The Godfather II"
		let movie3 = "John Wick"
		let movie4 = "One More Chance"
		let movie5 = "Fifty Shades of Black"
		let rating1 = "97%"
		let rating2 = "96%"
		let rating3 = "93%"
		let rating4 = "93%"
		let rating5 = "4%"

		console.log("1. " + movie1)
		console.log("Rotten Tomatoes Rating: " + rating1)
		console.log("2. " + movie2)
		console.log("Rotten Tomatoes Rating: " + rating2)
		console.log("3. " + movie3)
		console.log("Rotten Tomatoes Rating: " + rating3)
		console.log("4. " + movie4)
		console.log("Rotten Tomatoes Rating: " + rating4)
		console.log("5. " + movie5)
		console.log("Rotten Tomatoes Rating: " + rating5)
	}
	showMyFavoriteMovies()
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function /*printUsers*/(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2);